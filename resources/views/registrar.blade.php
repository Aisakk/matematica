@extends('layouts.app')

@section('content')
    <header>
        <div class="container">
            <nav>
                <ul>
                <li><a href="{{url('/')}}">Atras</a></li>
            </nav>  
        </div>
    </header>
    <body>
        <div class="container">
            <form action="{{ route('insertar')}}" method="post">
                @csrf
                <label for="">Producto</label>
                <input type="text" name="producto">
                <label for="">Precio</label>
                <input type="text" name="precio">
                <label for="">Cantidad</label>
                <input type="text" name="cantidad">
                <input type="submit" value="Registrar">
            </form>
        </div>
        <div class="container">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Id</th>
                  <th scope="col">Producto</th>
                  <th scope="col">Precio</th>
                  <th scope="col">Cantidad</th>
                </tr>
              </thead>
              <tbody>
               @foreach($prueba as $item)
                <tr>
                  <th scope="col">{{$item->id}}</th>
                  <th scope="col">{{$item->producto}}</th>
                  <th scope="col">{{$item->precio}}</th>
                  <th scope="col">{{$item->cantidad}}</th>
                </tr>
                @endforeach()
              </tbody>
            </table>
        </div>
    </body>
@endsection
