@extends('layouts.app')

@section('content')

    <div class="container">
          <nav>
              <ul>
              <li><a href="{{url('/')}}">Atras</a></li>
        </nav>  
    </div>
	<div class="container-form">
		<label for="">Producto</label>
		<input type="text">
		<label for="">Ventas</label>
		<input type="text">
		<label for="">En la semana o mes</label>
		<input type="text">
		<button>Progreso</button>
	</div>
    <div class="container-estadistica">
    	<canvas id="myChart" width="400" height="400"></canvas>
    </div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
<script>

	var ctx = document.getElementById('myChart').getContext('2d');
	var stackedLine = new Chart(ctx, {
    type: 'line',
  data: {
    labels: [0,10,20,30,40,50,60,70,80,90, 100],
    datasets: [ { 
        data: [0,10,20,30,40,50,60,70,80,90, 100],
        label: "Progress",
        borderColor: "#c45850",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Progress in my job'
    },
    legend:{
    	display: true,
    	position: 'top',
    	labels: {
	      boxWidth: 80,
	      fontColor: 'black'
	    }
    }
  }
});
</script>
@endsection

