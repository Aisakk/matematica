@extends('layouts.app')
<style>
    .menu-container{
        height: 250px;
        width: 250px;
        border-radius: 1px solid #cccc;
        margin: 0 auto;
    }
    .navegacion{
        margin: 30% 0%;
    }
    nav ul{
        list-style: none;
    }
    nav ul li{
        padding-top: 10px; 
    }
    nav ul li a{
        text-decoration: none;
        
    }
</style>
@section('content')
    <div class="menu-container">
          <nav class="navegacion">
              <ul>
              <li><a href="{{url('registrar')}}"> Registrar Producto</a></li>
              <li><a href="{{url('estadistica')}}"> Estadistica de Progreso</a></li>
              </ul>
        </nav>  
    </div>
@endsection